package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
	"strconv"
)

var err error
var data []byte
var output string
var base int = 64
var pad rune = '='
var padding string
var alt bool = false
var encode bool = true

func main() {
	if isPipe() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			data = append(data, scanner.Bytes()...)
		}
		err = scanner.Err()
		er(err)
	}

	args := os.Args[1:]

	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" || arg == "-h" {
			fmt.Print("-h | --help\tPrint this help message\n")
			fmt.Print("-f | --file\tFile to encode or decode (Default: STDIN)\n")
			fmt.Print("-d\t\tDecode (Default: Encode)\n")
			fmt.Print("-b <int>\tBase to use [16,32,64] (Default: 64)\n")
			fmt.Print("--alt\t\tEnable alternate encoding. b64: URL safe, b32: extended HEX, b16: lowercase (Default: Disabled)\n")
			return
		}

		if arg == "-d" {
			encode = false
		}

		if arg == "--alt" {
			alt = true
		}

		if (arg == "-f" || arg == "--file") && !isPipe() && n+1 < len(args) {
			filename := args[n+1]
			_, err = os.Stat(filename)
			er(err)

			fh, err := os.Open(filename)
			er(err)
			defer fh.Close()

			scanner := bufio.NewScanner(fh)
			for scanner.Scan() {
				data = append(data, scanner.Bytes()...)
			}
		}

		if arg == "-b" && n+1 < len(args) {
			tmpInt, err := strconv.Atoi(args[n+1])
			er(err)

			if tmpInt == 16 || tmpInt == 32 || tmpInt == 64 {
				base = tmpInt
			} else {
				er(fmt.Errorf("invalid base: %d", tmpInt))
			}

		}
	}

	if len(data) == 0 {
		er(fmt.Errorf("No data to process"))
	}

	convMap := make(map[int]rune)

	if base == 64 {
		for i := 0; i < 26; i++ {
			convMap[i] = rune(i + 65)
		}

		for i := 26; i < 52; i++ {
			convMap[i] = rune(i + 71)
		}

		for i := 52; i < 62; i++ {
			convMap[i] = rune(i - 4)
		}

		if alt {
			convMap[62] = '-'
			convMap[63] = '_'
		} else {
			convMap[62] = '+'
			convMap[63] = '/'
		}

		if len(data)%3 == 2 {
			padding = "="
		}

		if len(data)%3 == 1 {
			padding = "=="
		}

		for len(data)%3 != 0 {
			data = append(data, byte(0))
		}

		if encode {
			for i := 0; i < len(data); i++ {
				var inputData [4]byte

				if i%3 == 0 && i+2 < len(data) {
					inputData[0] = data[i] >> 2
					inputData[1] = data[i] << 6 >> 2
					inputData[1] = inputData[1] + data[i+1]>>4
					inputData[2] = data[i+1] << 4 >> 2
					inputData[2] = inputData[2] + data[i+2]>>6
					inputData[3] = data[i+2] << 2 >> 2

					output = output + string(convMap[int(inputData[0])]) + string(convMap[int(inputData[1])]) + string(convMap[int(inputData[2])]) + string(convMap[int(inputData[3])])
				}
			}

			if padding == "==" {
				tmpString := output[:len(output)-2]
				output = tmpString
			}

			if padding == "=" {
				tmpString := output[:len(output)-1]
				output = tmpString
			}

			output = output + padding

			fmt.Println(output)
		} else {
			reverseMap := make(map[rune]int)
			for k, v := range convMap {
				reverseMap[v] = k
			}

			for i := 0; i < len(data); i++ {
				if i%3 == 0 {
					output = output + string(reverseMap[rune(data[i]>>2)])
					output = output + string(reverseMap[rune(data[i]<<6>>6)])
				}

				if i%3 == 1 {
					output = output + string(reverseMap[rune(data[i]>>4)])
					output = output + string(reverseMap[rune(data[i]<<4>>4)])
				}

				if i%3 == 2 {
					output = output + string(reverseMap[rune(data[i]>>6)])
					output = output + string(reverseMap[rune(data[i]<<2>>2)])
				}
			}

			fmt.Println(output)
		}
	}

	if base == 32 {
		for i := 0; i < 26; i++ {
			convMap[i] = rune(i + 65)
		}

		for i := 26; i < 32; i++ {
			convMap[i] = rune(i + 24)
		}

		if alt {
			for i := 0; i < 10; i++ {
				convMap[i] = rune(i + 48)
			}

			for i := 10; i < 32; i++ {
				convMap[i] = rune(i + 55)
			}
		}

		if len(data)%5 == 1 {
			padding = "======"
		}

		if len(data)%5 == 2 {
			padding = "===="
		}

		if len(data)%5 == 3 {
			padding = "==="
		}

		if len(data)%5 == 4 {
			padding = "="
		}

		for len(data)%5 != 0 {
			data = append(data, byte(0))
		}

		if encode {
			for i := 0; i < len(data); i++ {
				var inputData [8]byte

				if i%5 == 0 && i+4 < len(data) {
					inputData[0] = data[i] >> 3
					inputData[1] = data[i] << 5 >> 3
					inputData[1] = inputData[1] + data[i+1]>>6
					inputData[2] = data[i+1] << 2 >> 3
					inputData[3] = data[i+1] << 7 >> 3
					inputData[3] = inputData[3] + data[i+2]>>4
					inputData[4] = data[i+2] << 4 >> 3
					inputData[4] = inputData[4] + data[i+3]>>7
					inputData[5] = data[i+3] << 1 >> 3
					inputData[6] = data[i+3] << 6 >> 3
					inputData[6] = inputData[6] + data[i+4]>>5
					inputData[7] = data[i+4] << 3 >> 3

					output = output + string(convMap[int(inputData[0])]) + string(convMap[int(inputData[1])]) +
						string(convMap[int(inputData[2])]) + string(convMap[int(inputData[3])]) + string(convMap[int(inputData[4])]) +
						string(convMap[int(inputData[5])]) + string(convMap[int(inputData[6])]) + string(convMap[int(inputData[7])])

				}

			}

			if padding == "======" {
				tmpString := output[:len(output)-6]
				output = tmpString
			}

			if padding == "====" {
				tmpString := output[:len(output)-4]
				output = tmpString
			}

			if padding == "===" {
				tmpString := output[:len(output)-3]
				output = tmpString
			}

			if padding == "=" {
				tmpString := output[:len(output)-1]
				output = tmpString
			}

			output = output + padding

			fmt.Println(output)
		} else {
			//decode
		}
	}

	if base == 16 {
		for i := 0; i < 10; i++ {
			convMap[i] = rune(i + 48)
		}

		for i := 10; i < 16; i++ {
			if !alt {
				convMap[i] = rune(i + 55)
			} else {
				convMap[i] = rune(i + 87)
			}
		}

		if encode {
			for i := 0; i < len(data); i++ {
				output = output + string(convMap[int(data[i]>>4)])
				output = output + string(convMap[int(data[i]<<4>>4)])
			}

			fmt.Println(output)
		} else {
			//decode
		}
	}

	/*	for k, v := range convMap {
			fmt.Printf("%d -> %s\n", k, string(v))
		}
	*/

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
